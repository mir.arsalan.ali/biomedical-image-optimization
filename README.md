# Biomedical Image Optimization

## Objectives
- Set up Fenics environment to process PDEs and inverse problems required for the project
- Integrate environment with available data for image quality optimization
- Solve for optimization problem
- Select optimum prior for enhanced image outputs
- Obtain optimal levels of noise without overfitting for best image quality

## Installation
For those with a windows OS, installing fenics through the windows subsystem for linux or docker is quite difficult. To work around this, I downloaded the Oracle VirtualBox. The VM allows you to run a virtual version of any linux OS on top of your windows OS. The OS running on the Oracle VM will be at near native performance as long as you allocate enough memory and hard drive space on your machine. The hard drive space is permanent until you delete the vm but the allocated memory is freed to your native OS once you shut down the VM. Here is the download page for the VM: https://www.virtualbox.org/wiki/Downloads

For those with a windows OS, you are going to click on "Windows hosts" under "VirtualBox 6.1.22 platform packages". This will download the installation wizard on your machine. The installation is pretty straight forward but here is a helpful guide if you run into any trouble: https://www.wikihow.com/Install-VirtualBox

Once you have installed the VM you can create a virtual OS of your choice. To use the fenics software, I created a VM with the latest version of Ubuntu. This will give you access to all Ubuntu features as if it was running natively on your computer. To start the Ubuntu VM for the first time, you must first download the ISO file from the Ubuntu site: https://ubuntu.com/download/desktop. This file will be the start up "disk" that the VM uses to install Ubuntu. Once you have downloaded the file, follow the instructions listed on this site, https://www.wikihow.com/Install-Ubuntu-on-VirtualBox, and you will have Ubuntu fully installed and ready to use.

How much hard drive space and memory to allocate, really depends on your machine and how many programs you plan to run on the VM. I would recommend at least 30 gigs of hard drive space but if you can spare it, over 50 gigs would be ideal.

Once Ubuntu is fully installed, you can install python through the command line: https://phoenixnap.com/kb/how-to-install-python-3-ubuntu. And when python is installed, you can install fenics using the Ubuntu commands on the fenics download page: https://fenicsproject.org/download/. And you will be all set to use the software!

However, for installation in Mac it is much simpler. Just visit the aformentioned link for Fenics project which essentially covers the basics for installation in Mac systems.

Other links for using Fenics project for solving PDEs have also been uploaded to the project.

