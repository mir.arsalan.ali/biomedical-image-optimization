import sys
import dolfin as dl
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

sys.path.append('/Users/arsalan/Downloads/Tim_hall_lab_Data/target1/')

from hippylib import nb

def makeMesh(nodes, cells, celltype, tdim, gdim, order=True):
    num_nodes = nodes.shape[0]
    num_elem = cells.shape[0]

    mesh = dl.Mesh()
    editor = dl.MeshEditor()
    editor = dl.MeshEditor()
    editor.open(mesh, celltype, tdim, gdim)
    editor.init_vertices(num_nodes)
    editor.init_cells(num_elem)

    [editor.add_vertex(i,n) for i,n in enumerate(nodes)]
    [editor.add_cell(i,n) for i,n in enumerate(cells)]
    editor.close(order=order)

    return mesh

filepath = '/Users/arsalan/Downloads/Tim_hall_lab_Data/target1/'
filename = 'disp_target1_frame_accu5.mat'
data = sio.loadmat(filepath+filename)

disp_x = data['disp_x']
disp_y = data['disp_y']

imshow(disp_x)
exit()

quadmesh = makeMesh(nodes, elem, 'quadrilateral', 2, 2, order=False)

dl.plot(quadmesh)
plt.show()

triConn = []
for el in elem:
    triConn.append([el[0], el[1], el[2]])
    triConn.append([el[0], el[2], el[3]])
​
trimesh = makeMesh(nodes, np.array(triConn), 'triangle', 2, 2)
with dl.XDMFFile(mesh.mpi_comm(), '../triangleMesh.xdmf') as xdmf:
    xdmf.write(trimesh)
plt.figure()
dl.plot(trimesh)
​
V = dl.VectorFunctionSpace(trimesh, "CG", 1)
u = dl.Function(V)
​
d2v_f = dl.dof_to_vertex_map(V)
u.vector()[:] = um.flatten()[d2v_f]
​
plt.figure(figsize=(14,5))
nb.plot(trimesh, subplot_loc=131, mytitle = 'mesh')
nb.plot(u.sub(0), subplot_loc=132, mytitle = '$u_x$')
nb.plot(u.sub(1), subplot_loc=133, mytitle = '$u_y$')
plt.show()
