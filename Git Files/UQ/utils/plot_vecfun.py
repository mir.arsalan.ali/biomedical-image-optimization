"""
plot components of vector functions
"""

import dolfin as dl
from hippylib import nb
#import pdb

def _getcomponents(f):
    fx = f.sub(0, deepcopy=True)
    fy = f.sub(1, deepcopy=True)

    return fx, fy

def plot(obj, titles, same_colorbar=False, show_axis='off', logscale=False, vmin=None, vmax=None, cmap=None):
    mesh = obj.function_space().mesh()
    assert isinstance(obj, dl.Function), "Must be a function"
    assert obj.value_rank() == 1, "Must be a vector field"
    assert mesh.topology().dim() == 2, "Mesh must be 2D"

#    if isinstance(obj, dl.Function):
    fx, fy = _getcomponents(obj)
    objs = [fx, fy]

    nb.multi1_plot(objs, titles, same_colorbar=same_colorbar, show_axis=show_axis, logscale=logscale, vmin=vmin, \
        vmax=vmax, cmap=cmap)

def multi_plot(obj, titles, same_colorbar=False, show_axis='off', logscale=False, vmin=None, vmax=None, cmap=None, comp=0):
    nobjs = len(obj)
    assert nobjs <= 3, "Too many figures"
    for i in range(nobjs):
        assert isinstance(obj[i], dl.Function), "Must be a function"

#    pdb.set_trace()
    if  obj[0].value_rank() is 1 and obj[1].value_rank() is 1:
        fx0, fy0  = _getcomponents(obj[0])
        fx1, fy1  = _getcomponents(obj[1])

        if comp is 0:
            objs = [fx0, fx1]
        elif comp is 1:
            objs = [fy0, fy1]
    elif obj[0].value_rank() is 1 and obj[1].value_rank() is 0:
        fx, fy = _getcomponents(obj[0])
        objs = [fx, fy, obj[1]]
    else:
        raise NotImplementedError("This case does not exist")

    nb.multi1_plot(objs, titles, same_colorbar=same_colorbar, show_axis=show_axis, logscale=logscale, vmin=vmin, \
        vmax=vmax, cmap=cmap)
