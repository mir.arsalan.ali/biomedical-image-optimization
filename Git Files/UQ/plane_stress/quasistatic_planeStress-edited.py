import dolfin as dl
import ufl
import math
import numpy as np
import matplotlib.pyplot as plt
import argparse

import sys
import os
sys.path.append( os.environ.get('HIPPYLIB_BASE_DIR', "../../") )
sys.path.append("../")
from hippylib import *
#from hippylib import nb
from misfitUSD import ContinuousStateObservationUSD
import scipy.io as sio

class GammaTop(dl.SubDomain):
    def inside(self, x, on_boundary):
        return dl.near(x[1], 1)

def point_boundary(x, on_boundary):
    return dl.near(x[0],28) and dl.near(x[1],46.2)


def all_boundary(x, on_boundary):
    return on_boundary

def addanisotropicnoise(V, u, noise_std_dev_x, noise_std_dev_y):
    ux_noise = u.copy()
    uy_noise = u.copy()
    parRandom.normal_perturb(noise_std_dev_x, ux_noise)
    parRandom.normal_perturb(noise_std_dev_y, uy_noise)
    indicator_x = dl.interpolate(dl.Constant((1, 0)), V).vector()
    indicator_y = dl.interpolate(dl.Constant((0, 1)), V).vector()
    u.zero()
    u.axpy(1., indicator_x*ux_noise)
    u.axpy(1., indicator_y*uy_noise)
    return u

def makeMesh(nodes, cells, celltype, tdim, gdim, order=True):
    num_nodes = nodes.shape[0]
    num_elem = cells.shape[0]

    mesh = dl.Mesh()
    editor = dl.MeshEditor()
    editor = dl.MeshEditor()
    editor.open(mesh, celltype, tdim, gdim)
    editor.init_vertices(num_nodes)
    editor.init_cells(num_elem)

    [editor.add_vertex(i,n) for i,n in enumerate(nodes)]
    [editor.add_cell(i,n) for i,n in enumerate(cells)]
    editor.close(order=order)

    return mesh

filepath = '/Users/arsalan/Downloads/Tim_hall_lab_Data/target1/'
filename = 'disp_target1_frame_accu5.mat'
data = sio.loadmat(filepath+filename)

full_disp_x = data['disp_x']
full_disp_y = data['disp_y']

full_disp_y*=0.01925
full_disp_x*=38/311

ds=4
disp_x= full_disp_x[0:300:ds,0:231:ds]
disp_y= full_disp_y[0:300:ds,0:231:ds]

nx=disp_x.shape[1]
ny=disp_x.shape[0]

nelem=(nx-1)*(ny-1)
conn=np.zeros((nelem,4),dtype=np.int32)
for i in range(ny-1):
    for j in range(nx-1):
        e=j+(nx-1)*i
        n=nx*i+j+1
        conn[e,:]=n,n+1,n+(nx+1),n+nx

nodes=np.zeros((nx*ny,2))


xv=np.linspace(0,28,nx)
yv=np.linspace(0,46.2,ny)
xc,yc=np.meshgrid(xv,yv)
nodes[:,0]=xc.flatten()
nodes[:,1]=yc.flatten()

conn -= 1

quadmesh = makeMesh(nodes, conn, 'quadrilateral', 2, 2, order=False)

triConn = []
for el in conn:
    triConn.append([el[0], el[1], el[2]])
    triConn.append([el[0], el[2], el[3]])

trimesh = makeMesh(nodes, np.array(triConn), 'triangle', 2, 2)
#with dl.XDMFFile(trimesh.mpi_comm(), '../triangleMesh.xdmf') as xdmf:
#    xdmf.write(trimesh)
#plt.figure()
#dl.plot(trimesh)
#plt.show()

nnodes=nx*ny
datamesh=np.zeros((nnodes,2))
datamesh[:,0]=disp_x.flatten()
datamesh[:,1]=disp_y.flatten()

mydata=np.zeros((2*nnodes,1))
mydata[:,0]=datamesh.flatten()

V = dl.VectorFunctionSpace(trimesh, "CG", 1)
u = dl.Function(V)

d2v_f = dl.dof_to_vertex_map(V)
u.vector()[:] = datamesh.flatten()[d2v_f]

plt.figure(figsize=(14,5))
nb.plot(trimesh, subplot_loc=131, mytitle = 'mesh')
nb.plot(u.sub(0), subplot_loc=132, mytitle = '$u_x$')
nb.plot(u.sub(1), subplot_loc=133, mytitle = '$u_y$')

#plt.show()

def epsilon(u):
    return ufl.sym(ufl.grad(u))

def A(u):
    return  dl.Constant(2.0)*(dl.tr(epsilon(u))*ufl.Identity(d) + epsilon(u))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Model Subsurface')
    parser.add_argument('--nx',
                        default=64,
                        type=int,
                        help="Number of elements in x-direction")
    parser.add_argument('--ny',
                        default=64,
                        type=int,
                        help="Number of elements in y-direction")
    parser.add_argument('--nsamples',
                        default=50,
                        type=int,
                        help="Number of samples from prior and Laplace Approximation")
    args = parser.parse_args()
    try:
        dl.set_log_active(False)
    except:
        pass
    sep = "\n"+"#"*80+"\n"
    ndim = 2
#    nx = args.nx
 #   ny = args.ny
    mesh = trimesh
  #  plt.figure()
  #  dl.plot(mesh)
  #  plt.show()
#    exit()
#    print()
#    exit()

    rank = dl.MPI.rank(mesh.mpi_comm())
    nproc = dl.MPI.size(mesh.mpi_comm())

    Vh2 = dl.VectorFunctionSpace(mesh, 'Lagrange', 1)
    Vh1 = dl.FunctionSpace(mesh, 'Lagrange', 1)
    Vh = [Vh2, Vh1, Vh2]

    ndofs = [Vh[STATE].dim(), Vh[PARAMETER].dim(), Vh[ADJOINT].dim()]
    if rank == 0:
        print (sep, "Set up the mesh and finite element spaces", sep)
        print ("Number of dofs: STATE={0}, PARAMETER={1}, ADJOINT={2}".format(*ndofs) )

    # Define the true parameter
    mtrue_expression = dl.Expression('std::log(2 + 8*(std::pow(std::pow(x[0]-0.5,2) + std::pow(x[1]-0.5,2),0.5) < 0.2))', \
                                      degree = 2)
    mtrue_fun = dl.interpolate(mtrue_expression, Vh[PARAMETER])
    mtrue = mtrue_fun.vector()

    # Initialize Expressions
    t = dl.Constant((0.0, 0.0))
    d = mesh.topology().dim()

 #   u_bdr = dl.Constant((0.0, 0.0))
    u_bdr0 = dl.Constant(0.0)
    bc_y = dl.DirichletBC(Vh[STATE].sub(1), u.sub(1), all_boundary)
    bc0_y = dl.DirichletBC(Vh[STATE].sub(1), u_bdr0, all_boundary)

    bc_x = dl.DirichletBC(Vh[STATE].sub(0), u.sub(0), point_boundary, method='pointwise')
    bc0_x = dl.DirichletBC(Vh[STATE].sub(0), u_bdr0, point_boundary, method='pointwise')

    bc0=[bc0_x,bc0_y]
    bc=[bc_x,bc_y]

    top = GammaTop()
    marker = dl.MeshFunction("size_t", mesh, d-1)
    marker.set_all(0)
    top.mark(marker, 1)
    dss = dl.Measure("ds", subdomain_data=marker)

    def pde_varf(u,m,p):
        return ufl.inner(p, t)*dss - ufl.exp(m)*ufl.inner(A(u), ufl.grad(p))*ufl.dx

    pde = PDEVariationalProblem(Vh, pde_varf, bc, bc0, is_fwd_linear=True)

#    pde.solver = PETScKrylovSolver(mesh.mpi_comm(), "cg", amg_method())
#    pde.solver_fwd_inc = PETScKrylovSolver(mesh.mpi_comm(), "cg", amg_method())
#    pde.solver_adj_inc = PETScKrylovSolver(mesh.mpi_comm(), "cg", amg_method())

#    pde.solver.parameters["relative_tolerance"] = 1e-15
#    pde.solver.parameters["absolute_tolerance"] = 1e-20
#    pde.solver_fwd_inc.parameters = pde.solver.parameters
#    pde.solver_adj_inc.parameters = pde.solver.parameters

    scale_prior = math.sqrt(1e-4)
    gamma = 1e-3
    delta = 1e-4
    gamma *= scale_prior
    delta *= scale_prior

    prior = BiLaplacianPrior(Vh[PARAMETER], gamma, delta, robin_bc=True)
    prior.mean.zero()
    area_d = dl.assemble((dl.Constant(0.0)*mtrue_fun + dl.Constant(1.0))*dl.dx)
    prior.mean += dl.assemble(mtrue_fun*dl.dx)/area_d

#    prior = TG_Prior(Vh[PARAMETER], gamma = 1e1, eps = 1e-4, mu = 1e-2, delta = 1e-2) 
            
    if rank == 0:
        print ( "Prior regularization: (delta_x - gamma*Laplacian)^order: delta={0}, gamma={1}, order={2}".format(delta, gamma,2) )   
                

    #Generate synthetic observations
#    utrue = pde.generate_state()
    #uobs = pde.generate_state()
#    x = [utrue, mtrue, None]
#   pde.solveFwd(x[STATE], x)
    uobs=u.vector()
    rel_noise_x = 0.1
    rel_noise_y = 0.01
    MAX = uobs.norm("linf")
    noise_std_dev_x = rel_noise_x * MAX
    noise_std_dev_y = rel_noise_y * MAX
    noise_variances = np.array([noise_std_dev_x**2, noise_std_dev_y**2])
    
    misfit = ContinuousStateObservationUSD(Vh[STATE], dl.dx, bc0, noise_variances)
#    misfit.d = addanisotropicnoise(Vh[STATE], utrue, noise_std_dev_x, noise_std_dev_y)
    misfit.d=uobs

    
    model = Model(pde, prior, misfit)
    
    if rank == 0:
        print( sep, "Test the gradient and the Hessian of the model", sep )
    
#    m0 = dl.interpolate(dl.Expression("sin(x[0])", element=Vh[PARAMETER].ufl_element() ), Vh[PARAMETER])
#    modelVerify(model, m0.vector(), is_quadratic = False, verbose = (rank == 0) )

    if rank == 0:
        print( sep, "Find the MAP point", sep)
    m = prior.mean.copy()
    parameters = ReducedSpaceNewtonCG_ParameterList()
    parameters["rel_tolerance"] = 1e-3
    parameters["abs_tolerance"] = 1e-12
    parameters["max_iter"]      = 30
    parameters["globalization"] = "LS"
    parameters["GN_iter"] = 5
    if rank != 0:
        parameters["print_level"] = -1
        
    if rank == 0:
        parameters.showMe()
    solver = ReducedSpaceNewtonCG(model, parameters)
    
    x = solver.solve([None, m, None])
    
    if rank == 0:
        if solver.converged:
            print( "\nConverged in ", solver.it, " iterations.")
        else:
            print( "\nNot Converged")

        print ("Termination reason: ", solver.termination_reasons[solver.reason])
        print ("Final gradient norm: ", solver.final_grad_norm)
        print ("Final cost: ", solver.final_cost)
        
    if rank == 0:
        print (sep, "Compute the low rank Gaussian Approximation of the posterior", sep)
    
    model.setPointForHessianEvaluations(x, gauss_newton_approx = False)
    Hmisfit = ReducedHessian(model, misfit_only=True)
    k = 50
    p = 20
    if rank == 0:
        print ("Double Pass Algorithm. Requested eigenvectors: {0}; Oversampling {1}.".format(k,p) )
    
    Omega = MultiVector(x[PARAMETER], k+p)
    parRandom.normal(1., Omega)

    d, U = doublePassG(Hmisfit, prior.R, prior.Rsolver, Omega, k, s=1, check=False)
    posterior = GaussianLRPosterior(prior, d, U)
    posterior.mean = x[PARAMETER]
    
    
    post_tr, prior_tr, corr_tr = posterior.trace(method="Randomized", r=200)
    if rank == 0:
        print ("Posterior trace {0:5e}; Prior trace {1:5e}; Correction trace {2:5e}".format(post_tr, prior_tr, corr_tr))

    post_pw_variance, pr_pw_variance, corr_pw_variance = posterior.pointwise_variance(method="Randomized", r=200)
        
    kl_dist = posterior.klDistanceFromPrior()
    if rank == 0:
        print ("KL-Distance from prior: ", kl_dist)
    
    with dl.XDMFFile(mesh.mpi_comm(), "results/pointwise_variance.xdmf") as fid:
        fid.parameters["functions_share_mesh"] = True
        fid.parameters["rewrite_function_mesh"] = False
    
        fid.write(vector2Function(post_pw_variance, Vh[PARAMETER], name="Posterior"), 0)
        fid.write(vector2Function(pr_pw_variance, Vh[PARAMETER], name="Prior"), 0)
        fid.write(vector2Function(corr_pw_variance, Vh[PARAMETER], name="Correction"), 0)

    if rank == 0:
        print (sep, "Save State, Parameter, Adjoint, and observation in paraview", sep)
    xxname = ["state", "parameter", "adjoint"]
    xx = [vector2Function(x[i], Vh[i], name=xxname[i]) for i in range(len(Vh))]
    
    with dl.XDMFFile(mesh.mpi_comm(), "results/results.xdmf") as fid:
        fid.parameters["functions_share_mesh"] = True
        fid.parameters["rewrite_function_mesh"] = False 
           
        fid.write(xx[STATE],0)
        fid.write(vector2Function(uobs, Vh[STATE], name = "observations"), 0)
        fid.write(xx[PARAMETER],0)
        fid.write(vector2Function(mtrue, Vh[PARAMETER], name = "true parameter"), 0)
        fid.write(vector2Function(prior.mean, Vh[PARAMETER], name = "prior mean"), 0)
        fid.write(xx[ADJOINT],0)
        
    if rank == 0:
        print( sep, "Generate samples from Prior and Posterior\n","Export generalized Eigenpairs", sep )

    nsamples = args.nsamples
    noise = dl.Vector()
    posterior.init_vector(noise,"noise")
    s_prior = dl.Function(Vh[PARAMETER], name="sample_prior")
    s_post = dl.Function(Vh[PARAMETER], name="sample_post")
    with dl.XDMFFile(mesh.mpi_comm(), "results/samples.xdmf") as fid:
        fid.parameters["functions_share_mesh"] = True
        fid.parameters["rewrite_function_mesh"] = False
        for i in range(nsamples):
            parRandom.normal(1., noise)
            posterior.sample(noise, s_prior.vector(), s_post.vector())
            fid.write(s_prior, i)
            fid.write(s_post, i)
        
    #Save eigenvalues for printing:
    
    U.export(Vh[PARAMETER], "results/evect.xdmf", varname = "gen_evects", normalize = True)
    if rank == 0:
        np.savetxt("results/eigevalues.dat", d)
        
    if rank == 0:
        plt.figure()
        plt.plot(range(0,k), d, 'b*', range(0,k), np.ones(k), '-r')
        plt.yscale('log')
        plt.show()    
        
