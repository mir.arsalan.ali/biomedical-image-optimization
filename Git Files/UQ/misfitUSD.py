# Copyright (c) 2016-2018, The University of Texas at Austin 
# & University of California--Merced.
# Copyright (c) 2019-2020, The University of Texas at Austin 
# University of California--Merced, Washington University in St. Louis.
#
# All Rights reserved.
# See file COPYRIGHT for details.
#
# This file is part of the hIPPYlib library. For more information and source code
# availability see https://hippylib.github.io.
#
# hIPPYlib is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License (as published by the Free
# Software Foundation) version 2.0 dated June 1991.

import dolfin as dl
import ufl
from hippylib import *
        
class ContinuousStateObservationUSD(Misfit):
    """
    This class implements continuous state observations for ultrasound data in a 
    subdomain :math:`X \subset \Omega` or :math:`X \subset \partial \Omega`.
    """
    def __init__(self, Vh, dX, bcs, noise_variances, form = None):
        """
        Constructor:

            :code:`Vh`: the finite element space for the state variable.
            
            :code:`dX`: the integrator on subdomain `X` where observation are presents. \
            E.g. :code:`dX = ufl.dx` means observation on all :math:`\Omega` and :code:`dX = ufl.ds` means observations on all :math:`\partial \Omega`.
            
            :code:`bcs`: If the forward problem imposes Dirichlet boundary conditions :math:`u = u_D \mbox{ on } \Gamma_D`;  \
            :code:`bcs` is a list of :code:`dolfin.DirichletBC` object that prescribes homogeneuos Dirichlet conditions :math:`u = 0 \mbox{ on } \Gamma_D`.
            
            :code:`noise_variances`: array of noise variances. Array length must be same as dimension of problem.
            
            :code:`form`: if :code:`form = None` we compute the :math:`L^2(X)` misfit: :math:`\int_X (u - u_d)^2 dX,` \
            otherwise the integrand specified in the given form will be used.
        """
        if form is None:
            u, v = dl.TrialFunction(Vh), dl.TestFunction(Vh)
            self.W = dl.assemble(ufl.inner(u,v)*dX)
        else:
            self.W = dl.assemble( form )
           
        if bcs is None:
            bcs  = []
        if isinstance(bcs, dl.DirichletBC):
            bcs = [bcs]
            
        if len(bcs):
            Wt = Transpose(self.W)
            [bc.zero(Wt) for bc in bcs]
            self.W = Transpose(Wt)
            [bc.zero(self.W) for bc in bcs]
                
        self.d = dl.Vector(self.W.mpi_comm())
        self.vec = dl.Vector(self.W.mpi_comm())
        self.W.init_vector(self.d,1)
        self.W.init_vector(self.vec,1)
        self.op = _mult_comp(Vh, noise_variances, self.W)
        
    def cost(self,x):
        r = self.d.copy()
        r.axpy(-1., x[STATE])
        self.vec.zero()
        self.op.mult(r, self.vec)
        
        Wr = dl.Vector(self.W.mpi_comm())
        self.W.init_vector(Wr,0)
        self.W.mult(r,Wr)
        return self.vec.inner(Wr)/2.
    
    def grad(self, i, x, out):
        if i == STATE:
            self.W.mult(x[STATE]-self.d, out)
            self.vec.zero()
            self.op.mult(out, self.vec)
            out.zero()
            out.axpy(1., self.vec)
        elif i == PARAMETER:
            out.zero()
        else:
            raise IndexError()
        
    def setLinearizationPoint(self,x, gauss_newton_approx=False):
        # The cost functional is already quadratic. Nothing to be done here
        return
                   
    def apply_ij(self,i,j,dir,out):
        if i == STATE and j == STATE:
            self.W.mult(dir, out)
            self.vec.zero()
            self.op.mult(out, self.vec)
            out.zero()
            out.axpy(1., self.vec)
        else:
            out.zero() 
            
class _mult_comp:
    """
    Operator that multiplies each component of state by (possibly different) noise variance
    """
    def __init__(self, Vh, noise_variances, W):
        """
        Constructor:
            :code:`Vh`: the finite element space for the state variable.
            :code:`noise_variances`: array of noise variances. Array length must be same as dimension of problem.
            :code:`W`:  mass matrix for state variable
        """
        self.Vh = Vh
        self.dimp = Vh.mesh().geometry().dim()
        self.noise_variances = noise_variances
        assert self.noise_variances.size == self.dimp, 'Need variance array to have same length as dimension of problem'
        assert (self.noise_variances > 0).all(), 'Only positive variances are allowed'
        self.indicator_vec = self._init_indicator_vec(W)
        
    def _init_indicator_vec(self, W):
        vec = dl.Vector(W.mpi_comm())
        W.init_vector(vec, 1)
        indicator_vec = MultiVector(vec, self.dimp)
        
        for i in range(self.dimp):
            indicator_list = [0]*self.dimp
            indicator_list[i] = 1
            indicator_vec[i].axpy(1., dl.interpolate(dl.Constant(indicator_list), self.Vh).vector())

        return indicator_vec

    def mult(self, y, z):
        for i in range(self.dimp):
            z.axpy(1./self.noise_variances[i], self.indicator_vec[i]*y)

