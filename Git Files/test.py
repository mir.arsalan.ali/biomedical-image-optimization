import sys
import dolfin as dl
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

sys.path.append('/Users/arsalan/Downloads/Tim_hall_lab_Data/target1/')

from hippylib import nb

def makeMesh(nodes, cells, celltype, tdim, gdim, order=True):
    num_nodes = nodes.shape[0]
    num_elem = cells.shape[0]

    mesh = dl.Mesh()
    editor = dl.MeshEditor()
    editor = dl.MeshEditor()
    editor.open(mesh, celltype, tdim, gdim)
    editor.init_vertices(num_nodes)
    editor.init_cells(num_elem)

    [editor.add_vertex(i,n) for i,n in enumerate(nodes)]
    [editor.add_cell(i,n) for i,n in enumerate(cells)]
    editor.close(order=order)

    return mesh

filepath = '/Users/arsalan/Downloads/Tim_hall_lab_Data/target1/'
filename = 'disp_target1_frame_accu5.mat'
data = sio.loadmat(filepath+filename)

full_disp_x = data['disp_x']
full_disp_y = data['disp_y']


ds=4
disp_x= full_disp_x[0:300:ds,0:231:ds]
disp_y= full_disp_y[0:300:ds,0:231:ds]
#print(disp_x.shape)



#print(nx)
#print(ny)
#exit()

nx=disp_x.shape[1]
ny=disp_x.shape[0]

nelem=(nx-1)*(ny-1)
conn=np.zeros((nelem,4),dtype=np.int32)
for i in range(ny-1):
    for j in range(nx-1):
        e=j+(nx-1)*i
        n=nx*i+j+1
        conn[e,:]=n,n+1,n+(nx+1),n+nx

#print(conn)
#exit()
nodes=np.zeros((nx*ny,2))


xv=np.linspace(0,28,nx)
yv=np.linspace(0,46.2,ny)
xc,yc=np.meshgrid(xv,yv)
nodes[:,0]=xc.flatten()
nodes[:,1]=yc.flatten()

#print(nodes)
#exit()

#elem = data['elem']
conn -= 1             #node numbering starts at 0 in python
#nodes = data['nodes']
#um = data['um']

#plt.imshow(disp_x)
#plt.show()



quadmesh = makeMesh(nodes, conn, 'quadrilateral', 2, 2, order=False)


#dl.plot(quadmesh)
#plt.show()

triConn = []
for el in conn:
    triConn.append([el[0], el[1], el[2]])
    triConn.append([el[0], el[2], el[3]])

trimesh = makeMesh(nodes, np.array(triConn), 'triangle', 2, 2)
with dl.XDMFFile(trimesh.mpi_comm(), '../triangleMesh.xdmf') as xdmf:
    xdmf.write(trimesh)
#plt.figure()
#dl.plot(trimesh)
#plt.show()

nnodes=nx*ny
datamesh=np.zeros((nnodes,2))
datamesh[:,0]=disp_x.flatten()
datamesh[:,1]=disp_y.flatten()

mydata=np.zeros((2*nnodes,1))
mydata[:,0]=datamesh.flatten()

#exit()

#read data into here
V = dl.VectorFunctionSpace(trimesh, "CG", 1)
u = dl.Function(V)

d2v_f = dl.dof_to_vertex_map(V)
u.vector()[:] = datamesh.flatten()[d2v_f]

plt.figure(figsize=(14,5))
nb.plot(trimesh, subplot_loc=131, mytitle = 'mesh')
nb.plot(u.sub(0), subplot_loc=132, mytitle = '$u_x$')
nb.plot(u.sub(1), subplot_loc=133, mytitle = '$u_y$')
plt.show()




#2*3 nodes
#for i in range (1):
    # ...:     for j in range (2):
    # ...:         e=j+2*i
    # ...:         n=i+1+2*j
    # ...:         conn[e,:]=n,n+1,n+3,n+2

#3*2 nodes
# for i in range (2):
#     ...:     for j in range (1):
#     ...:         e=j+i
#     ...:         n=i+1+j
#     ...:         conn[e,:]=n,n+1,n+4,n+3

#4*3 nodes
#for i in range (3):
#    ...:     for j in range (2):
#   ...:         e=i+3*j
#    ...:         n=4*j+i+1
#    ...:         conn[e,:]=n,n+1,n+5,n+4

#3*4
#for i in range (2):
#    ...:     for j in range (3):
#    ...:         e=i+2*j
#    ...:         n=3*j+i+1
#    ...:         conn[e,:]=n,n+1,n+4,n+3

#4*4
#In [63]: for i in range (3):
    # ...:     for j in range (3):
    # ...:         e=i+3*j
    # ...:         n=4*j+i+1
    # ...:         conn[e,:]=n,n+1,n+5,n+4








